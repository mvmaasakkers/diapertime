angular.module('starter.services', [])

.factory('authService', ['$http', '$q', function ($http, $q) {

  var authServiceFactory = {
    auth: false,
    person: {},
    profiles: {
      "martijn":  {
        "displayName": "Martijn",
        "emailaddress": "martijn@vanmaasakkers.net"
      },
      "marleen":  {
        "displayName": "Marleen",
        "emailaddress": "mabastiaans@hotmail.com"
      }
    }
  };


  authServiceFactory.login = function login(name) {
    if(authServiceFactory.profiles.hasOwnProperty(name)) {
      authServiceFactory.person = authServiceFactory.profiles[name];
      authServiceFactory.auth = true;
      
      return 
    }
    
    authServiceFactory.auth = false;
    authServiceFactory.person = {};
  }
  
  return authServiceFactory;
}]);
